/// \file appmem.h
///
/// Declaration of the memory subsystem.
/// see: http://www.swedishcoding.com/2008/08/31/are-we-out-of-memory/
#ifndef APPMEM_H
#define APPMEM_H

#include <cstdlib>
#include "../core/system.h"
#include "../core/allocator/linear-allocator.h"
#include "../core/util/singleton.h"
#include <list>

/// \class MemoryPool
///
/// Manages a preallocated memory pool.
class MemoryPool : public ISystem {
	DEF_SINGLETON(MemoryPool);
public:
	// Claims a memory region for an allocator.
	void* claim(size_t size_bytes);

	// sets allocators that need to be reset every frame.
	void add_per_frame_allocator(LinearAllocator& allocator) {
		m_frame_allocators.push_back(&allocator); }
	
	void initialize();
	void finalize();
	void update(float dt);

private:
	void* m_pool;
	size_t m_pool_size;
	LinearAllocator* m_allocator;
	std::list<LinearAllocator*> m_frame_allocators;
};

#endif // appmem.h