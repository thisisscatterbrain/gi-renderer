/// \file appmem.cpp
#include "appmem.h"
#include "../core/report.h"
#include "../core/settings.h"

MemoryPool::MemoryPool() {}

MemoryPool::~MemoryPool() {}

void MemoryPool::initialize()
{
	Settings* settings = Settings::get();
	m_pool_size = settings->get("appmem/size")->as_int() * 1024 * 1024;
	m_pool = malloc(m_pool_size);

	if (!m_pool)
		die("Failed to allocate memory pool (%d Mb)", m_pool_size / (1024 * 1024));

	m_allocator = new LinearAllocator(m_pool_size, m_pool);
}

void MemoryPool::finalize()
{
	free(m_pool);
	delete m_allocator;
}

void MemoryPool::update(float dt)
{
	for (LinearAllocator* allocator : m_frame_allocators)
		allocator->reset();
}