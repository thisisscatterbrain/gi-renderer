#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "core/application.h"
#include "memory/appmem.h"

static void pause()
{
	system("pause");
}

struct A {
	size_t member[2];
};

int main(int argc, char *argv[])
{
	atexit(pause);
	Application* app = Application::get();
	app->add_system(MemoryPool::get());
	app->initialize();
	app->run();
	app->finalize();
	return EXIT_SUCCESS;
}