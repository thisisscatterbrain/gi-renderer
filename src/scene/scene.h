/// \file scene.h
///
/// A scene is implemented by the scene system. A scene consists of components
/// such as Mesh, PointLight, etc ... . Components are stored and managed by
/// their manager classes. Each component (manager) is identified by an unique
/// id. Components are connected hierachically by a scene graph.
#ifndef SCENE_H
#define SCENE_H

#include "../core/system.h"
#include "../core/util/singleton.h"
#include "../core/allocator/linear-allocator.h"
#include "manager.h"

class Scene : public ISystem {
	DEF_SINGLETON(Scene)
public:
	// Component identifier
	enum {
		SCENE_MESH = 0,
		SCENE_POINT_LIGHT,
		SCENE_COMPONENT_COUNT
	};

	// ISystem implementations
	void initialize();
	void finalize();
	void update(float dt);

private:
	LinearAllocator scene_allocator;
};

#endif // scene.h