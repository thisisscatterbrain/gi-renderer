template <class COMPONENT>
Manager<COMPONENT>::Manager(int type, size_t max_component_count)
	: AManager(type), m_allocator(max_component_count) {}

template <class COMPONENT>
size_t Manager<COMPONENT>::get_component_count() const
{
	return m_allocator.m_current_object_count;
}