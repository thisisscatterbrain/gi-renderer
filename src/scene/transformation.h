// transformation.h
#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

class Transformation {
public:
	Transformation();
	~Transformation();

	const float* get_transformation() const;
	void set_scale(const float scale[3]);
	void set_translation(const float translation[3]);
	
private:
	void recompute() const;
	
	float translation[3];
	float rotation[4];
	float scale[3];
	
	mutable float tranformation[16];
	mutable int dirty;
};

#endif // transformation