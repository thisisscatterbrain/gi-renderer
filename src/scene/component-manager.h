/// \file manager.h
#ifndef COMPONENT_MANAGER_H
#define COMPONENT_MANAGER_H

#include "../core/allocator/linear-allocator.h"

class Manager : protected LinearAllocator {
public:
	/// Constructs a component manager with a memory pool.
	Manager(int type, size_t max_elem_count, size_t elem_size);
	~Manager();

	int get_type() const {return m_type;}

private:
	int	m_type;
	size_t	m_element_size;
	size_t	m_max_count;
};

template <class COMPONENT>
class ComponentManager : public Manager {
public:
	ComponentManager(int type, size_t max_element_count);
	~ComponentManager();

	/// Allocates a new COMPONENT object.
	COMPONENT* allocate();

	/// Gets a component for an index
	const Component* get(size_t id) const;

private:
	size_t m_count;
};

#endif // manager.h