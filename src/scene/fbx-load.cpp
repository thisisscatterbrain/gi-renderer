#include "fbx-load.h"
#include "point-light.h"
#include "mesh.h"
#include "../core/report.h"
#include <fbxsdk.h>
#include <cassert>
#include <map>

//------------------------------------------------------------------------------
// Creates a fbx manager and loads a scene from the file specified by
// "filename".
static int load_fbx_scene(FbxManager** manager, FbxScene** scene,
	const std::string& filename);

static void add_children(Node* node, const FbxNode* fbxnode);

//------------------------------------------------------------------------------

Node* fbx_load(const std::string& filename)
{
	FbxManager * manager;
	FbxScene * scene;
	
	if (!load_fbx_scene(&manager, &scene, filename)) {
		print_warning("failed to load file : %s", filename.c_str());
		return nullptr;
	}
	
	// create a root node
	FbxNode* fbxroot = scene->GetRootNode();
	Node* root = new Node(std::string(fbxroot->GetName()));
	
	// convert fbx nodes and store them in our scene graph
	add_children(root, scene->GetRootNode());

	// clean up
	manager->Destroy();

	return root;
}

//------------------------------------------------------------------------------

static int load_fbx_scene(FbxManager** manager, FbxScene** scene,
	const std::string& filename)
{
	*manager = FbxManager::Create();
	FbxIOSettings *ios = FbxIOSettings::Create(*manager, IOSROOT);
	(*manager)->SetIOSettings(ios);
	FbxImporter *importer = FbxImporter::Create(*manager, "");
	
	if(!importer->Initialize(filename.c_str(), -1,
		(*manager)->GetIOSettings())) {
		(*manager)->Destroy();
		return 0;
	}
	
	*scene = FbxScene::Create(*manager, "");
	importer->Import(*scene);
	importer->Destroy();
	
	// Triangulate all meshes in the scene.
	FbxGeometryConverter conv(*manager);
	conv.Triangulate(*scene, true);
	return 1;
}

//------------------------------------------------------------------------------

// Sets the transformation of a node given a fbx node.
static void set_transformation(Node* node, const FbxNode* fbxnode)
{
	float transl[3];
	transl[0] = (float)fbxnode->LclTranslation.Get()[0];
	transl[1] = (float)fbxnode->LclTranslation.Get()[1];
	transl[2] = (float)fbxnode->LclTranslation.Get()[2];

	float scale[3];
	scale[0] = (float)fbxnode->LclScaling.Get()[0];
	scale[1] = (float)fbxnode->LclScaling.Get()[1];
	scale[2] = (float)fbxnode->LclScaling.Get()[2];

	// TODO: Rotation!

	node->get_transformation().set_translation(transl);
	node->get_transformation().set_scale(scale);
}

//------------------------------------------------------------------------------

// Converts a fbx pointlight and adds it to "parent".
static Node* add_point_light(Node* parent, const FbxNode* fbxnode)
{
	FbxLight* light = (FbxLight *)fbxnode->GetNodeAttribute();
	
	if (FbxLight::ePoint != light->LightType.Get()) {
		return nullptr;
	}
	
	float col[3];
	col[0] = (float)light->Color.Get()[0] * light->Intensity.Get();
	col[1] = (float)light->Color.Get()[1] * light->Intensity.Get();
	col[2] = (float)light->Color.Get()[2] * light->Intensity.Get();
	
	PointLight* pl = new PointLight(std::string(fbxnode->GetName()), col);
	set_transformation(pl, fbxnode);
	parent->add_child(pl);
	return pl;
}

//------------------------------------------------------------------------------

static void convert_material_lamb(Material* mat,
	const FbxSurfaceMaterial* fbxmat)
{
	FbxSurfaceLambert* m = (FbxSurfaceLambert*)fbxmat;
	
	mat->diffuse_reflectance[0] = (float)m->Diffuse.Get()[0];
	mat->diffuse_reflectance[1] = (float)m->Diffuse.Get()[1];
	mat->diffuse_reflectance[2] = (float)m->Diffuse.Get()[2];
}

static void convert_material_phong(Material* mat,
	const FbxSurfaceMaterial* fbxmat)
{
	FbxSurfacePhong* m = (FbxSurfacePhong*)fbxmat;
	
	mat->diffuse_reflectance[0] = (float)m->Diffuse.Get()[0];
	mat->diffuse_reflectance[1] = (float)m->Diffuse.Get()[1];
	mat->diffuse_reflectance[2] = (float)m->Diffuse.Get()[2];
}

static void convert_material(Material* mat, const FbxSurfaceMaterial* fbxmat)
{
	if (fbxmat->GetClassId().Is(FbxSurfacePhong::ClassId)) {
		convert_material_phong(mat, fbxmat);
	} else if (fbxmat->GetClassId().Is(FbxSurfaceLambert::ClassId)) {
		convert_material_lamb(mat, fbxmat);
	} else {
		print_warning("Undefined material");
	}
}

// Converts a FbxVector4 to a float[3] array.
static void convert_vector(float v[3], const FbxVector4* fbxv)
{
	v[0] = (float)(*fbxv)[0];
	v[1] = (float)(*fbxv)[1];
	v[2] = (float)(*fbxv)[2];
}

// Reads the normal from a fbx mesh converts it to a float[3] and stores it in
// "normal".
static void get_normal(float normal[3], int vertex_id, const FbxMesh * mesh)
{
	if (mesh->GetElementNormalCount() > 1)
		print_warning("Mesh has more than one normal per vertex. Currently only the first normal is read, ignoring the others.");

	int id = 0;
	const FbxGeometryElementNormal* normals = mesh->GetElementNormal(0);

	// check normal mapping
	if (normals->GetMappingMode() != FbxGeometryElement::eByPolygonVertex)
		die("Normals should be mapped to each vertex");

	switch (normals->GetReferenceMode()) {
	case FbxGeometryElement::eDirect:
		convert_vector(normal, 
			&normals->GetDirectArray().GetAt(vertex_id));
		break;
	case FbxGeometryElement::eIndexToDirect:
		id = normals->GetIndexArray().GetAt(vertex_id);
		convert_vector(normal,
			&normals->GetDirectArray().GetAt(id));
		break;
	default:
		die("Reference mode not supported.");
	}

}

// Converts a fbx mesh to a mesh and adds it to "parent". An fbx mesh might
// contain faces with different materials assigned to them, we split up 
// such meshes per material so that we can render them easier in OpenGL.
// Each mesh is added to "parent", and the mesh that is added the last
// is returned as a child.
static Node* create_meshes(Node* parent, const FbxNode* fbxnode)
{
	FbxMesh* mesh = (FbxMesh*)fbxnode->GetNodeAttribute();
	int npolys = mesh->GetPolygonCount();
	FbxVector4* controlpoints = mesh->GetControlPoints();
	
	// We use a hash map that maps a fbx material to a Mesh object
	// to split up meshes with different materials ...
	std::map<FbxSurfaceMaterial*, Mesh*> matmeshmap;
	
	// We require a mesh to possess normals.
	mesh->GenerateNormals();
	
	for (int i = 0; i < npolys; i++) {
		for (int j = 0; j < mesh->GetPolygonSize(i); j++) {

			// assure that we only get triangles.
			assert(mesh->GetPolygonSize(i) == 3);

			// ignore polys with no material
			if (mesh->GetElementMaterialCount() == 0) {
				print_warning("Ignoring polygon with no material.");
				continue;
			}
			
			// Currently we support only one "material layer".
			if (mesh->GetElementMaterialCount() > 1)
				print_warning("currently only one material layer supported...");
			
			// 1. Get material
			FbxGeometryElementMaterial* matelem =
				mesh->GetElementMaterial(0);
			FbxSurfaceMaterial* mat =
				mesh->GetNode()->GetMaterial(
					matelem->GetIndexArray().GetAt(i));
			
			// If we cannot find a mesh for a fbx mat., create a
			// new one.
			if (matmeshmap.find(mat) == matmeshmap.end()) {
				matmeshmap[mat] = new Mesh(
					std::string(fbxnode->GetName()));

				// set the mesh's material
				convert_material(&matmeshmap[mat]->material,
					mat);
			
				// set the transformation of the mesh
				set_transformation(matmeshmap[mat], fbxnode);
			}
			
			// 2. Store the poly geometry in the mesh that corresponds
			// to the material.
			int idx = mesh->GetPolygonVertex(i, j);
			float pos[3], norm[3];
			convert_vector(pos, &controlpoints[idx]);
			get_normal(norm, i * 3 + j /* the vertex id .. */, mesh);

			matmeshmap[mat]->push_vertex(pos, norm);			
		}
	}
	
	if (matmeshmap.empty())
		return nullptr;

	// add the meshes from the map to the parent as child.
	Mesh* latest = nullptr;
	for (auto& matmesh : matmeshmap) {
		latest = matmesh.second;
		parent->add_child(latest);
	}

	return latest; // lets say the latest mesh added to "parent" is the actual child ...
}

//------------------------------------------------------------------------------

// Creates a mesh, light, camera, ... node from the fbx node and adds it
// to our parent node as a child.
// Returns the latest child added to "parent".
static Node* add_child(Node* parent, const FbxNode* fbxnode)
{
	if (!fbxnode)
		return nullptr;

	print_info(fbxnode->GetName());
	
	const FbxNodeAttribute* attr = fbxnode->GetNodeAttribute();
	assert(attr);
	
	switch (attr->GetAttributeType()) {
	case FbxNodeAttribute::eLight:
		return add_point_light(parent, fbxnode);
	case FbxNodeAttribute::eMesh:
		return create_meshes(parent, fbxnode);
	default:
		break;
	}
	
	return nullptr;
}

// Converts children of fbxnode to a a Node and adds them to a parent "node".
static void add_children(Node* node, const FbxNode* fbxnode)
{
	int nchilds = fbxnode->GetChildCount();
	
	for (int i = 0; i < nchilds; i++) {
		Node* child = add_child(node, fbxnode->GetChild(i));

		if (child)
			add_children(child, fbxnode->GetChild(i));
		else
			add_children(node, fbxnode->GetChild(i));
	}
}