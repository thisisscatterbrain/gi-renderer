// transformation.cpp
#include "transformation.h"
#include <memory>
#include <am/matrix4.h>

Transformation::Transformation() : dirty(0)
{
	memset(this->translation, 0, sizeof(this->translation));
	memset(this->rotation, 0, sizeof(this->rotation));

	this->scale[0] = 1.0;
	this->scale[1] = 1.0;
	this->scale[2] = 1.0;

	am_matrix4_make_scale(this->tranformation, this->scale);
}

Transformation::~Transformation() {}

void Transformation::set_scale(const float* scale)
{
	memcpy(this->scale, scale, sizeof(this->scale));
	this->dirty = 1;
}

void Transformation::set_translation(const float* translation)
{
	memcpy(this->translation, translation, sizeof(this->translation));
	this->dirty = 1;
}

const float* Transformation::get_transformation() const
{
	if (dirty) {
		this->recompute();
		this->dirty = 0;
	}
	
	return this->tranformation;
}

void Transformation::recompute() const
{
	float transl[16];
	float scale[16];
	am_matrix4_make_scale(scale, this->scale);
	am_matrix4_make_translation(transl, this->scale);
	am_matrix4_multiply(this->tranformation, transl, scale);
}