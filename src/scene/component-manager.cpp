#include "component-manager.h"

Manager::Manager(int type, size_t max_elem_count, size_t elem_size) 
	: LinearAllocator(max_elem_count * elem_size), m_type(type),
	m_max_count(max_elem_count), m_element_size(elem_size) {}
Manager::~Manager() {}

