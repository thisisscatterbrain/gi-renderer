// node.h
//
// Nodes make up the scene graph of the scene. They can hold references to 
// other components.
#ifndef NODE_H
#define NODE_H

#include <list>
#include <map>
#include "transformation.h"
//#include "component.h"

#define NODE_MAX_NAME_LENGTH 32

class Node : public Component {
public:	
	struct Parameters {
		const char *name;
		int type;	
		Component *ref;	

		Parameters(const char *name, int type, Component *ref) :
			name(name), type(type), ref(ref) {}
	};
	
	int get_type() const {return m_type;}
	const char * get_name() const {return m_name;}
	Transformation& get_transformation() {return m_transformation;}
	const std::list<Node *> get_children() const {return *m_children;}
	void add_child(Node *node) {m_children->push_back(node);}

	void initialize(void *params);
	void finalize();

private:
	Node() {};
	virtual ~Node() {};

	char m_name[NODE_MAX_NAME_LENGTH];
	
	// The component type the node holds a reference to, or -1 if the node holds
	// no reference.
	int m_type;

	// Pointer to the component the node is referencing or NULL if the node holds
	// no reference
	Component* m_reference;

	// Local transformation for the node
	Transformation m_transformation;

	// List of children. Note that we have to dynamically allocate the list
	// as it is not initilized properly, due to the component manager, which only
	// allocates memory and not calls any constructors.
	std::list<Node *> *m_children; // TODO: custom list?
};


#endif // Node.h