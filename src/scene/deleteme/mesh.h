// mesh.h
//
// Meshes store scene geometry.
#ifndef MESH_H
#define MESH_H

#include "node.h"
//#include "component.h"
#include <vector>

struct Material {
	Material();
	~Material();
	
	float diffuse_reflectance[3];
};

class Mesh : public Component {
public:
	static const int TYPE;

	Mesh() {};
	virtual ~Mesh() {};
	
	int get_face_count() const {return (int)positions.size() / 9;}
	void push_vertex(const float position[3], const float normal[3]);
	void set_material(const Material& material) {this->material = material;}

private:
	std::vector<float> positions;
	std::vector<float> normals;
	
	Material material;
};

#endif // mesh.h