// node.cpp
#include "node.h"
#include <cstring>

void Node::initialize(void *params)
{
	Parameters *p = static_cast<Parameters*>(params);

	if (!p)
		return;

	if (p->name)
		strncpy(m_name, p->name, NODE_MAX_NAME_LENGTH);
	m_type = p->type;
	m_reference = p->ref;

	m_children = new std::list<Node *>();
}

void Node::finalize()
{
	delete m_children;
}