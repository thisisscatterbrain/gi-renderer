// mesh.cpp
#include "mesh.h"
#include <memory>

const int Mesh::TYPE = 2;

Mesh::Mesh(const std::string& name) : Node(TYPE, name) {}
Mesh::~Mesh() {}

void Mesh::push_vertex(const float position[3], const float normal[3])
{
	for (int i = 0; i < 3; i++) {
		this->positions.push_back(position[i]);
		this->normals.push_back(normal[i]);
	}
}

Material::Material()
{
	memset(this->diffuse_reflectance, 0, sizeof(this->diffuse_reflectance));
}

Material::~Material() {};