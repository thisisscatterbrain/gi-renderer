/// \file point-light.h
#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H

#include "../core/object.h"

class PointLight {
	DEF_OBJECT(PointLight)
public:
	void inititialize_with_parameters(const float radiance[3]);
	
	float m_radiance[3];
};

#endif // point-light.h