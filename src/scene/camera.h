// camera.h
#ifndef CAMERA_H
#define CAMERA_H

#include "node.h"

namespace fxs {

class Camera : public Node {
public:
	static const int TYPE;	
	
	Camera(int type, float position[3], float focus[3], float up[3]);
	virtual ~Camera();
	
	const float * get_world_to_camera() const {return this->world_to_camera;}

private:
	float world_to_camera[16];
};
	
}

#endif // camera.h