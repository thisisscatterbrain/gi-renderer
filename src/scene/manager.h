/// \file manager.h
//#ifndef MANAGER_H
//#define MANAGER_H

#pragma once

#include "../core/allocator/linear-allocator.h"

class AManager {
public:
	AManager(int type) : m_type(type) {};
	virtual ~AManager() {};
	int get_type() const {return m_type;}

	virtual size_t get_component_count() const = 0;

private:
	int m_type;
};

template <class COMPONENT>
class Manager : public AManager {
public:
	Manager(int type, size_t max_component_count);
	virtual ~Manager() { reset(m_allocator); }

	size_t get_component_count() const;

	LinearAllocator<COMPONENT>* get_allocator() { return &m_allocator; }
private:
	LinearAllocator<COMPONENT> m_allocator;
};

#include "manager.inl"

//#endif // manager.h