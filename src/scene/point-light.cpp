// point-light.cpp
#include "point-light.h"
#include <memory>

void PointLight::inititialize_with_parameters(const float radiance[3])
{
	memcpy(m_radiance, radiance, sizeof(m_radiance));
}

void PointLight::release() {}
