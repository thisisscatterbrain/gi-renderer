// fbx-load.h
//
// Loads data from a .fbx file.
#ifndef FBX_LOAD_H
#define FBX_LOAD_H

#include "node.h"
#include <string>

Node * fbx_load(const std::string &filename);

#endif // fbx-load.h