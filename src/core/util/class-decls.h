/// \file class-decls
///
/// Convenience makros for class declerations.
#ifndef CLASS_DECLS_H
#define CLASS_DECLS H

#define DEF_NO_COPY(name)			\
	name(const name&);			\
	name& operator=(const name&);	

#endif // class-decls.h