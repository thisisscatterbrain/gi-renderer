// singleton.h
//
// Defines a makro for making classes a singleton
#ifndef SINGLETON_H
#define SINGLETON_H

#define DEF_SINGLETON( NAME )		\
public:					\
	static NAME* get()		\
        {				\
		static NAME instance;	\
		return &instance;       \
	}				\
private:				\
	NAME();				\
	NAME(const NAME&);		\
	NAME& operator=(const NAME&);	\
	virtual ~NAME();

#endif // singleton.h