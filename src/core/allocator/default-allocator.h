/// \file default-allocator.h
#ifndef DEFAULT_ALLOCATOR_H
#define DEFAULT_ALLOCATOR_H

#include "allocator.h"
#include "../util/singleton.h"

class DefaultAllocator : public Allocator {
	DEF_SINGLETON(DefaultAllocator)
public:
	void* allocate(size_t elem_size_bytes, uint8_t alignnment = 4);
	void deallocate(void* p);
};

#endif // default-allocator.h