/// \file linear-allocator.h
#ifndef LINEAR_ALLOCATOR_H
#define LINEAR_ALLOCATOR_H

#include "allocator.h"

/// \class linear-allocator
///
/// Allocates memory sequentially. Deallocations are ignored, "reset()" can be 
/// used to reset the allocator to its initial state, which effectively invalidates
/// all the memory previously allocated.
class LinearAllocator : public Allocator {
	DEF_NO_COPY(LinearAllocator)
public:
	/// Constructs a linear allocator with a local memory pool of "size_bytes"
	/// at its disposal.
	LinearAllocator(size_t size_bytes);

	/// Constructs a linear allocator with a local memory pool of "size_bytes"
	/// at its disposal. "start" is a pointer to a preallocated memory pool.
	LinearAllocator(size_t size_bytes, void* start);
	~LinearAllocator();

	/// Allocates "size_bytes" memory alignt with "alignment".
	void* allocate(size_t size_bytes, uint8_t alignment);
	
	/// Deallocates memory pointed to by "p".
	void deallocate(void* p);

	/// Resets the linear allocator.
	void reset();

private:
	void* m_marker; // Points to the next free memory region.
};

#endif // linear allocator