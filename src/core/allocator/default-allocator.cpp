#include "default-allocator.h"
#include <cstdlib>

DefaultAllocator::DefaultAllocator() {}
DefaultAllocator::~DefaultAllocator() {}

void* DefaultAllocator::allocate(size_t elem_size_bytes, uint8_t alignnment)
{
	m_allocation_count++;
	return calloc(1, elem_size_bytes);
}

void DefaultAllocator::deallocate(void* object)
{
	m_allocation_count--;
	free(object);
}