void* Allocator::align_forward(void* address, uint8_t alignment)
{
	assert(alignment && (alignment & (alignment - 1) == 0));
	return (void*)((reinterpret_cast<uintptr_t>(address) + static_cast<uintptr_t>(alignment - 1)) & static_cast<uintptr_t>(~(alignment - 1)));
}

uint8_t Allocator::get_adjustment(void* address, uint8_t alignment)
{
	uint8_t adjustment =  alignment - static_cast<uint8_t>((reinterpret_cast<uintptr_t>(address) & static_cast<uintptr_t>(alignment - 1)));
	
	// already aligned
	if (adjustment == alignment)
		return 0;

	return adjustment;
}

uint8_t Allocator::get_adjustment_with_header(void* address, uint8_t alignment,
	uint8_t header_size_bytes)
{
	uint8_t adjustment = get_adjustment(address, alignment);

	if (header_size_bytes < adjustment)
		return adjustment;

	//Increase adjustment to fit header
	uint8_t needed = header_size_bytes - adjustment;
	adjustment += alignment * (needed / alignment);

	if ((needed % alignment) > 0)
		adjustment += alignment;

	return adjustment;
}
//-----------------------------------------------------------------------------

template <class T>
T* allocate(Allocator& allocator)
{
	return new (allocator.allocate(sizeof(T), __alignof(T))) T;
}

template <class T>
void deallocate(Allocator& allocator, T& object)
{
	object.~T();
	allocator.deallocate(&object);
}


template <class T>
T* allocate_array(Allocator& allocator, size_t length)
{
	assert(length);
	uint8_t headersize = sizeof(size_t) / sizeof(T);

	if (sizeof(size_t) % sizeof(T) > 0)
		headersize += 1;

	//Allocate extra space to store array length in the bytes before the array
	T* p = ((T*)allocator.allocate(sizeof(T)*(length + headersize), __alignof(T))) + headersize;

	*(((size_t*)p) - 1) = length;

	for (size_t i = 0; i < length; i++)
		new (&p[i]) T;

	return p;
}

template<class T>
void deallocate_array(Allocator& allocator, T* array)
{
	assert(array != nullptr);

	size_t length = *(((size_t*)array) - 1);

	for (size_t i = 0; i < length; i++)
		array[i].~T();

	//Calculate how much extra memory was allocated to store the length before the array
	uint8_t headersize = sizeof(size_t) / sizeof(T);

	if (sizeof(size_t) % sizeof(T) > 0)
		headersize += 1;

	allocator.deallocate(array - headersize);
}
