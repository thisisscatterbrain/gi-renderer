template <class OBJECT>
ElementAllocator<OBJECT>::ElementAllocator(size_t max_object_count,
	uint8_t alignment) : m_start(nullptr), m_adjusted_start(nullptr),
	m_max_object_count(max_object_count), m_element_size(sizeof(OBJECT)),
	m_alignment(alignment)
{
	if ((alignment & (alignment - 1)) != 0)
		die("Alignment is not power of two");

	m_start = static_cast<uint8_t*>(malloc(max_object_count * sizeof(OBJECT) + alignment));
	m_adjusted_start = m_start;

	uintptr_t startaddress = reinterpret_cast<uintptr_t>(m_start);

	if (!alignment)
		return;

	size_t adjustment = alignment - startaddress % alignment;

	if (adjustment != alignment)
		m_adjusted_start += adjustment;

	adjustment = alignment - m_element_size % alignment;

	if (adjustment != alignment)
		m_element_size += adjustment;
}

template <class OBJECT>
ElementAllocator<OBJECT>::~ElementAllocator()
{
	free(m_start);
}
