/// \file object-allocator.h
///
/// Allocates memory for objects of the same type.
#ifndef OBJECT_ALLOCATOR_H
#define OBJECT_ALLOCATOR_H

#include <cstdlib>
#include <cstdint>
#include "../report.h"

template<class OBJECT>
class ElementAllocator {
public:
	/// Initializes the allocator with with the total number of objects
	/// it should be able to contain.
	ElementAllocator(size_t max_object_count, uint8_t alignment = 4);
	
	/// Invalidates the memory of the allocator and deletes it.
	~ElementAllocator();

	uint8_t* m_start;
	uint8_t* m_adjusted_start;
	size_t m_max_object_count;
	size_t m_element_size;
	uint8_t m_alignment;
};


#include "object-allocator.inl"

#endif // object-allocator.h