/// \file linear-allocator.h
#ifndef LINEAR_ALLOCATOR_H
#define LINEAR_ALLOCATOR_H

#include "object-allocator.h"

/// \class LinearAllocator
///
/// Allocates objects in linear memory. All objects are tightly packed if no
/// alignment is set, otherwise they are arranged according to the alignment.
template <class OBJECT>
class LinearAllocator : public ElementAllocator<OBJECT> {
public:
	LinearAllocator(size_t size_bytes, uint8_t alignment = 4);
	~LinearAllocator();

	size_t m_current_object_count;
};

//-----------------------------------------------------------------------------

template<class OBJECT>
OBJECT* allocate(LinearAllocator<OBJECT>& allocator);

/// Resets the allocator. Note all objects allocated by the allocator will
/// become invalid and may be overwritten by subsequent calls to alloc.
template<class OBJECT>
void reset(LinearAllocator<OBJECT>& allocator);

/// Resets the allocator and calls the releas function of OBJECT. Note all
/// objects allocated by the allocator will become invalid and may be overwritten 
/// by subsequent calls to alloc.
template<class OBJECT>
void release(LinearAllocator<OBJECT>& allocator);

/// Gets the object at for the "index-th" memory block. Take care that
/// "index" < current count of allocated objects 
template<class OBJECT>
OBJECT* get(LinearAllocator<OBJECT>& allocator, size_t index);


#include "linear-allocator.inl"

#endif // linear-allocator.h