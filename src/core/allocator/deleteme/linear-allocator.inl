template <class OBJECT>
LinearAllocator<OBJECT>::LinearAllocator(size_t size_bytes, uint8_t alignment) :
ElementAllocator<OBJECT>(size_bytes, alignment),
m_current_object_count(0) {}

template <class OBJECT>
LinearAllocator<OBJECT>::~LinearAllocator()
{
	if (m_current_object_count)
		print_warning("Unreleased objects. Please call reset() before destroying an allocator.");
}

template <class OBJECT>
OBJECT* allocate(LinearAllocator<OBJECT>& allocator)
{
	uint8_t* ret = allocator.m_adjusted_start 
		+ allocator.m_current_object_count * allocator.m_element_size;

	if (allocator.m_current_object_count == allocator.m_max_object_count)
		die("Out of memory.");

	allocator.m_current_object_count++;
	OBJECT* objectptr = reinterpret_cast<OBJECT*>(ret);
	return objectptr;
}

/// Gets the object at for the "index-th" memory block. Take care that
/// "index" < current count of allocated objects 
template<class OBJECT>
OBJECT* get(LinearAllocator<OBJECT>& allocator, size_t index)
{
	return reinterpret_cast<OBJECT*>(allocator.m_adjusted_start + 
		index * allocator.m_element_size);
}

template <class OBJECT>
void reset(LinearAllocator<OBJECT>& allocator)
{
	allocator.m_current_object_count = 0;
}

template <class OBJECT>
void release(LinearAllocator<OBJECT>& allocator)
{
	for (size_t i = 0; i < allocator.m_current_object_count; i++)
		get(allocator, i)->release();

	allocator.m_current_object_count = 0;
}

