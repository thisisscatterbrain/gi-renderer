#include "allocator.h"

Allocator::Allocator(size_t size_bytes) : m_size_bytes(size_bytes),
	m_used_memory(0), m_allocation_count(0) 
{
	m_start = malloc(size_bytes);
	
	if (!m_start)
		die("Failed to allocated requested amount of memory %ld (bytes)", m_size_bytes);
}

Allocator::Allocator(size_t size_bytes, void* start) : m_start(start), 
	m_size_bytes(size_bytes), m_used_memory(0), m_allocation_count(0) {}

Allocator::~Allocator()
{
	if (m_allocation_count || m_used_memory)
		print_warning_ex(__FILE__, __LINE__, "Allocator still has used memory");
	
	m_start		= nullptr;
	m_size_bytes	= 0;
}