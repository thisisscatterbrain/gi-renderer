/// \file allocator.h
///
/// The allocator interface
/// see: http://www.gamedev.net/page/resources/_/technical/general-programming/c-custom-memory-allocation-r3010
#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <cstdint>
#include <new>
#include <cassert>
#include "../report.h"
#include "../util/class-decls.h"

#define IS_POW2(x) ((x != 0) && (x & (x -1)) == 0)

class Allocator {
	DEF_NO_COPY(Allocator)
public:
	Allocator() {};

	/// Constructs an allocator with a local memory pool of "size_bytes"
	/// at its disposal.
	Allocator(size_t size_bytes);

	/// Constructs an allocator with a local memory pool of "size_bytes"
	/// at its disposal. "start" is a pointer to a preallocated memory pool.
	Allocator(size_t size_bytes, void* start);
	virtual ~Allocator();

	virtual void* allocate(size_t elem_size_bytes, uint8_t alignnment) = 0;
	virtual void deallocate(void* p) = 0;

protected:
	inline void* align_forward(void* address, uint8_t alignment);
	
	/// Gets the adjustment that needs to be applied to an address to align
	/// it according to "alignment".
	inline uint8_t get_adjustment(void* address, uint8_t alignment);
	
	/// Gets the adjustment that needs to be applied to an address to align
	/// it according to "alignment". The adjustment is big enough to store
	/// a header of size "header_size_bytes".
	inline uint8_t get_adjustment_with_header(void* address, 
		uint8_t alignment, uint8_t header_size_bytes);

	void*	m_start;
	size_t	m_size_bytes;
	size_t	m_used_memory;
	size_t	m_allocation_count;	
};

template <class T>
T* allocate(Allocator& allocator);

template <class T>
void deallocate(Allocator& allocator, T& object);

template <class T>
T* allocate_array(Allocator& allocator, size_t length);

template<class T>
void deallocate_array(Allocator& allocator, T* array);

#include "allocator.inl"

#endif // allocator.h