/// \file linear-allocator.h
#include "linear-allocator.h"

LinearAllocator::LinearAllocator(size_t size_bytes)
	: Allocator(size_bytes) 
{
	m_marker = m_start;
}

LinearAllocator::LinearAllocator(size_t size_bytes, void* start)
	: Allocator(size_bytes, start), m_marker(start) {}

LinearAllocator::~LinearAllocator() 
{
	m_marker = nullptr;
}

void* LinearAllocator::allocate(size_t size_bytes, uint8_t alignment)
{
	if (!size_bytes || !IS_POW2(alignment))
		return nullptr;

	uint8_t adjustment = get_adjustment(m_marker, alignment);

	if (m_used_memory + adjustment + size_bytes > m_size_bytes)
		return nullptr;

	uintptr_t alignedaddress = reinterpret_cast<uintptr_t>(m_marker) + 
		adjustment;

	m_marker = reinterpret_cast<void*>(alignedaddress + size_bytes);
	m_used_memory += (size_bytes + adjustment);
	m_allocation_count++;

	return reinterpret_cast<void*>(alignedaddress);
}

void LinearAllocator::deallocate(void* p)
{
	print_warning_ex(__FILE__, __LINE__, "Linear allocator does not allocate");
}

void LinearAllocator::reset()
{
	m_allocation_count = 0;
	m_used_memory = 0;
	m_marker = 0;
}