// application.h
//
// Application skeleton
#ifndef APPLICATION_H
#define APPLICATION_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstdint>
#include <list>
#include "util/singleton.h"
#include "system.h"

class Application {
	DEF_SINGLETON(Application)
public:
	void initialize();
	void run();
	void finalize();

	void add_system(ISystem* system);

private:
	std::list<ISystem *> m_systems;

	GLFWwindow* m_window;
	int32_t m_is_initialized;
};

#endif // application.h