// application.cpp
#include "application.h"
#include "report.h"
#include "settings.h"
#include "report.h"

Application::Application() : m_is_initialized(0) {}
Application::~Application() {}

void Application::initialize()
{
	if (m_is_initialized)
		return;

	Settings* settings = Settings::get();
	
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	this->m_window = glfwCreateWindow(
		settings->get("core/window/width")->as_int(),
		settings->get("core/window/height")->as_int(), 
		settings->get("core/window/title")->as_c_string(),
		NULL, NULL);
	glfwMakeContextCurrent(this->m_window);

	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();
	
	if (!this->m_window)
		die("Failed to create the window.");

	// initialize your systems.
	for (ISystem *system : m_systems) {
		system->initialize();
	}

	
}

void Application::run()
{
	while (!glfwWindowShouldClose(this->m_window)) {
		
		for (ISystem *system : m_systems) {
			system->update(0.0); // TODO: pass time
		}

		glfwSwapBuffers(this->m_window);
		glfwPollEvents();
	}
}


void Application::finalize()
{
	glfwTerminate();

	for (ISystem *system : m_systems) {
		system->finalize();
	}
}

void Application::add_system(ISystem *system)
{
	if (m_is_initialized) {
		print_warning("Already initialized. System not added.");
		return;
	}
	
	if (system)
		m_systems.push_back(system);
	else 
		print_warning("[system] is nullptr.");
}