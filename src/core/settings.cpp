/// \file settings.cpp
#include "settings.h"
#include "report.h"

Settings::Settings()
{
	load("settings.json");
}

Settings::~Settings() {}

const Setting* Settings::get(const std::string& key) const
{
	if (m_settings.find(key) == m_settings.end())
		die("Setting %s not found", key.c_str());

	return &m_settings.find(key)->second;
}

void Settings::load(const std::string& filename)
{
	m_settings["core/window/title"] = Setting("Reflective Shadow Maps");
	m_settings["core/window/width"] = Setting("1280");
	m_settings["core/window/height"] = Setting("720");

	// Memory settings (in MB)
	m_settings["appmem/size"] = Setting("50");
}