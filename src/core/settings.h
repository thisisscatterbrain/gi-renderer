/// \file settings.h
#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <map>
#include "util/singleton.h"

/// \class Setting
/// Represents one setting.
class Setting {
public:
	Setting() : m_value("") {};
	Setting(const std::string& value) : m_value(value) {}
	~Setting() {};

	int as_int() const { return std::stoi(m_value); }
	long as_long() const { return std::stol(m_value); }
	float as_float() const { return std::stof(m_value); }
	float as_double() const { return std::stod(m_value); }
	const std::string* as_string() const { return &m_value; }
	const char* as_c_string() const { return m_value.c_str(); }
private:
	std::string m_value;
};

/// \class Settings
///
/// Loads and stores settings.
class Settings {
	DEF_SINGLETON(Settings);
public:
	void initialize();
	void finalize();

	const Setting* get(const std::string& key) const;
private:
	void load(const std::string& filename);

	std::map<std::string, Setting> m_settings;

};

#endif // setting.h