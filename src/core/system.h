// system.h
//
// Declares the system interface. Systems initialize themselves from the global
// "Settings" class, are updated every frame, and clean up after themselves.
#ifndef SYSTEM_H
#define SYSTEM_H

class ISystem {
public:
	virtual void initialize() = 0;
	virtual void finalize() = 0;
	virtual void update(float dt) = 0;
};

#endif // system.h